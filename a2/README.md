> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Valeria Puchaicela

### Assignment 2 Requirements:

*Includes:*

1. Screenshots of Healthy Recipes app (first and second user interface(s))
2. Bitbucket read-only access to lis4381 repo




#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface


#### Assignment Screenshots:

*Screenshot of running application's first user interface*:

![First user interface screenshot](img/main.png)

*Screenshot of running application's second user interface*:

![Second user interface screenshot](img/activity.png)


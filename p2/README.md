> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Valeria Puchaicela

### Project #2 Requirements: 1.Create server-side validation with edit and delete functions

#### Project #2 Screenshots:

Screenshot of the main page:
![Page1 Screenshot](img/main.png)

Screenshot of the edit page:

![Page2 Screenshot](img/edit.png)

Screenshot of the error page:

![Page3 Screenshot](img/p2error.png)

Screenshot of the rssfeed:

![Page3 Screenshot](img/rss.png)

Screenshot of the carousel:

![Page3 Screenshot](img/carousel.png)

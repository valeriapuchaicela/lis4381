> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Valeria Puchaicela

### Assignment #4 Requirements: 1.Create an online portfolio

#### Assignment #4 Screenshots:

Screenshot of the main page:
![Page1 Screenshot](img/mainslide.png)

Screenshot of the second page:

![Page2 Screenshot](img/passedvalidation.png)

Screenshot of the input:

![Input Screenshot](img/failedvalidation.png)
# LIS 4381 - Mobile Web Application Development

## Valeria Puchaicela

### Assignment Requirements:

*Course Work Links (Relative link example):*

1. [A1 README.md](a1/README.md) "My A1 README.md file"
    - Install AMPPS 
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md) "My A2 README.md file" 
    - Create Healthy Recipes app on Adroid Studio
    - Provide screenshots of running app

3. [A3 README.md](a3/README.md) "My A3 README.md file" 
    - Provide screenshot of ERD
    - Provide screenshot of running application's first user interface;
    - Provide screenshot of running application's second user interface;
    - Provide links to the following files:
        a. a3.mwb
        b. a3.sql

4. [P1 README.md](p1/README.md) "My P1 README.md file" 
    - Create My Business Card app on Adroid Studio
    - Provide screenshots of running application's first user interface;
    - Provide screenshot of running application's second user interface;
    
5. [A4 README.md](a4/README.md) "My A4 README.md file"
    - cd to local repos subdirectory
    - Clone assignment starter files
    - Review subdirectories and files
    - Open index.php and review code
    - Create a favicon using *your* initials, and place it in each assignment’s main directory, including lis4381
    - Include screenshots of main page, failed validation, and passed validation

6. [A5 README.md](a5/README.md) "My A5 README.md file"
    - Create server-side validation and prepared statements
    - Successfuly displays user-entered data
    - Permits user to add data
    - Include screenshots of index and error pages

7.  [P2 README.md](p2/README.md) "My P2 README.md file"
    - Create server-side validation and prepared statements
    - Successfuly displays user-entered data
    - Permits user to delete and edit data
    - Include screenshots of index, error, and edit pages
    - Include screenshot of carousel


    
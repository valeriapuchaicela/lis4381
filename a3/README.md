> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Valeria Puchaicela

### Assignment 3 Requirements:

*Includes:*

1. Screenshot of ERD
2. Screenshot of app interfaces
2. Bitbucket read-only access to lis4381 repo




#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of app interfaces
* Links to associated files


#### Assignment Screenshots:

*Screenshot of ERD*:

![Activity Screenshot](img/a3.png)

*Link to MWB file*:
![Link to MWB file](a3.mwb)